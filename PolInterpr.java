package lav;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by Lavi on 3/11/2017.
 */
public class PolInterpr implements ActionListener {

    Panel panel;
    String[] monoame1;
    String[] monoame2;


    public PolInterpr(Panel panel) { this.panel= panel;}


    public void actionPerformed (ActionEvent e) {

        String command = e.getActionCommand();



        if (command.equals("plus")) {

            String buffer1 = panel.polinom1.getText();
            String buffer2 = panel.polinom2.getText();
            Polinom polsum = this.sum(buffer1, buffer2);
            panel.statusLabel1.setText("<html><p style='font-family:Arial;font-weight:125;line-height:15px;'>Polinom 3: <b>" + polsum.afisare() + "</b></p></html>");

        } else if (command.equals("minus")) {

            String buffer1 = panel.polinom1.getText();
            String buffer2 = panel.polinom2.getText();
            Polinom poldif = this.dif(buffer1, buffer2);
            panel.statusLabel2.setText("<html><p style='font-family:Arial;font-weight:125;line-height:15px;'>Polinom 4: <b>" + poldif.afisare() + "</b></p></html>");

        } else if (command.equals("inmultire")) {

            String buffer1 = panel.polinom1.getText();
            String buffer2 = panel.polinom2.getText();
            Polinom polinmul = this.inmul(buffer1, buffer2);
            panel.statusLabel3.setText("<html><p style='font-family:Arial;font-weight:125;line-height:15px;'>Polinom 5: <b>" + polinmul.afisare() + "</b></p></html>");

        }

        else if (command.equals("impartire")) {

            String buffer1 = panel.polinom1.getText();
            String buffer2 = panel.polinom2.getText();
            ArrayList <Polinom> polis = new ArrayList<Polinom>();
            polis = this.impartire(buffer1,buffer2);
            panel.statusLabel4.setText("<html><p style='font-family:Arial;font-weight:125;line-height:15px;'>Polinom 6: <b>" + polis.get(0).afisare() + "  |   " + polis.get(1).afisare() + "</b></p></html>");
        }
    }


        private Polinom crearePol(String buffer) {

            String monoame[];
            String finalBuffer;

            //Verificam daca este vorba despre o derivata/integrala

        if (buffer.toLowerCase().contains("deriv")) {

            //se deriveaza
            buffer.trim().replace("deriv","");
            String actualBuffer = (buffer.contains("-")) ? buffer.replace("-","+-"): buffer;
            finalBuffer = (actualBuffer.charAt(0) == '+')  ? actualBuffer.substring(1) : actualBuffer;
            monoame = finalBuffer.split("\\+");
            Polinom poli = new Polinom(monoame,panel.x);
            poli.deriv();
            return poli;

        }

        else if (buffer.toLowerCase().contains("int")) {

            //se integreaza
            buffer.replace("int","").trim();
            String actualBuffer = (buffer.contains("-")) ? buffer.replace("-","+-") : buffer;
            finalBuffer = (actualBuffer.charAt(0) == '+') ? actualBuffer.substring(1) : actualBuffer;
            monoame = finalBuffer.split("\\+");
            Polinom poli = new Polinom(monoame,panel.x);
            poli.integ();
            return poli;

        }

        else {

            String actualBuffer = (buffer.contains("-")) ? buffer.replace("-","+-") : buffer;
            finalBuffer = (actualBuffer.charAt(0) == '+') ? actualBuffer.substring(1) : actualBuffer;
            monoame = finalBuffer.split("\\+");
            Polinom poli = new Polinom(monoame,panel.x);
            return poli;

        }
    }

    private Polinom sum(String buffer1, String buffer2) {

            Polinom poli1,poli2,poli3;
            poli1 = crearePol(buffer1);
            poli2 = crearePol(buffer2);
            poli3 = poli1.sum(poli2);
            return poli3;

    }


    private  Polinom dif(String buffer1, String buffer2) {

        Polinom poli1,poli2,poli3;
        poli1 = crearePol(buffer1);
        poli2 = crearePol(buffer2);
        poli3 = poli1.dif(poli2);
        return poli3;

    }



    private Polinom inmul(String buffer1, String buffer2) {

        Polinom poli1,poli2,poli3;
        poli1 = crearePol(buffer1);
        poli2 = crearePol(buffer2);
        poli3 = poli1.inmul(poli2);
        return poli3;

    }

    private ArrayList<Polinom> impartire(String buffer1,String buffer2){

        Polinom poli1,poli2;
        ArrayList<Polinom> polis = new ArrayList<Polinom>();
        poli1 = crearePol(buffer1);
        poli2 = crearePol(buffer2);
        polis = poli1.impartire(poli2);
        return  polis;
    }




}
