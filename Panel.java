package lav;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Created by Lavi on 3/11/2017.
 */
public class Panel {

    public JFrame mainFrame;
    public JLabel controlLabel1, statusLabel1,
                  controlLabel2, statusLabel2,
                  controlLabel3, statusLabel3,
                  controlLabel4, statusLabel4;
    public JPanel pol1,pol2, mesaje;
    public JTextField polinom1,polinom2,valoare;
    public JButton plus,minus,inmultire,impartire;
    public int x;


    public Panel() {
        prepGUI();
    }

    private void prepGUI() {


        mainFrame= new JFrame("Calcul Polinoame");
        mainFrame.setSize(500,600);
        mainFrame.setLayout(new GridLayout(1,1 ));
        JPanel column = new JPanel();
        mainFrame.add(column);

        //TextFields

        pol1 = new JPanel();
        pol1.setLayout(new FlowLayout());
        pol2 = new JPanel();
        pol2.setLayout(new FlowLayout());

        polinom1 = new JTextField(20);
        polinom2 = new JTextField(20);

        pol1.setBorder(BorderFactory.createMatteBorder(1,5,1,1,Color.black));
        pol1.add(new JLabel("Polinom 1: ",JLabel.CENTER));
        pol1.add(polinom1);



        pol2.setBorder(BorderFactory.createMatteBorder(1,5,1,1,Color.black));
        pol2.add(new JLabel("Polinom 2: ",JLabel.CENTER));
        pol2.add(polinom2);


        controlLabel1 = new JLabel("",JLabel.CENTER);
        controlLabel1.setPreferredSize(new Dimension(350,50));
        controlLabel1.setText("Suma: ");
        statusLabel1 = new JLabel("",JLabel.CENTER);
        statusLabel1.setVerticalTextPosition(JLabel.TOP);
        statusLabel1.setPreferredSize(new Dimension(350,50));
        JPanel child_mess1 = new JPanel();
        child_mess1.setLayout(new GridLayout(2,1));
        child_mess1.add(controlLabel1);
        child_mess1.add(statusLabel1);
        child_mess1.setBorder(BorderFactory.createMatteBorder(1,5,1,1,Color.gray));



        controlLabel2 = new JLabel("",JLabel.CENTER);
        controlLabel2.setPreferredSize(new Dimension(350,50));
        controlLabel2.setText("Diferenta: ");
        statusLabel2 = new JLabel("",JLabel.CENTER);
        statusLabel2.setVerticalTextPosition(JLabel.TOP);
        statusLabel2.setPreferredSize(new Dimension(350,50));
        JPanel child_mess2 = new JPanel();
        child_mess2.setLayout(new GridLayout(2,1));
        child_mess2.add(controlLabel2);
        child_mess2.add(statusLabel2);
        child_mess2.setBorder(BorderFactory.createMatteBorder(1,5,1,1,Color.gray));



        controlLabel3 = new JLabel("",JLabel.CENTER);
        controlLabel3.setPreferredSize(new Dimension(350,50));
        controlLabel3.setText("Inmultire: ");
        statusLabel3 = new JLabel("",JLabel.CENTER);
        statusLabel3.setVerticalTextPosition(JLabel.TOP);
        statusLabel3.setPreferredSize(new Dimension(350,50));
        JPanel child_mess3 = new JPanel();
        child_mess3.setLayout(new GridLayout(2,1));
        child_mess3.add(controlLabel3);
        child_mess3.add(statusLabel3);
        child_mess3.setBorder(BorderFactory.createMatteBorder(1,5,1,1,Color.gray));


        controlLabel4 = new JLabel("",JLabel.CENTER);
        controlLabel4.setPreferredSize(new Dimension(350,50));
        controlLabel4.setText("Impartire: ");
        statusLabel4 = new JLabel("",JLabel.CENTER);
        statusLabel4.setVerticalTextPosition(JLabel.TOP);
        statusLabel4.setPreferredSize(new Dimension(350,50));
        JPanel child_mess4 = new JPanel();
        child_mess4.setLayout(new GridLayout(2,1));
        child_mess4.add(controlLabel4);
        child_mess4.add(statusLabel4);
        child_mess4.setBorder(BorderFactory.createMatteBorder(1,5,1,1,Color.gray));


        //Butoane

        JPanel butoane = new JPanel();
        GridLayout but = new GridLayout(1,4);
        but.setHgap(20);
        butoane.setLayout(but);
        plus = Panel.creareButon("+");
        minus = Panel.creareButon("-");
        inmultire = Panel.creareButon("x");
        impartire = Panel.creareButon("/");
        butoane.add(plus);
        butoane.add(minus);
        butoane.add(inmultire);
        butoane.add(impartire);


        column.add(pol1);
        column.add(pol2);
        column.add(butoane);
        column.add(child_mess1);
        column.add(child_mess2);
        column.add(child_mess3);
        column.add(child_mess4);
        column.setVisible(true);
    }

    private static JButton creareButon(String text) {

        JButton button = new JButton("<html><p style='font-size:16px;font-family:Arial;'>"+text+"</p></html>");
        button.setForeground(Color.black);
        button.setBackground(Color.lightGray);
        Border line = BorderFactory.createLineBorder(Color.darkGray);
        Border margin = new EmptyBorder(5,15,5,15);
        Border compound = new CompoundBorder(line,margin);
        button.setBorder(compound);

        return button;
    }
     public void showEvent() {


        plus.setActionCommand("plus");
        minus.setActionCommand("minus");
        inmultire.setActionCommand("inmultire");
        impartire.setActionCommand("impartire");

        //Trimitem instante ale obiectelor catre PolInterpr


         plus.addActionListener(new PolInterpr(this));
         minus.addActionListener(new PolInterpr(this));
         inmultire.addActionListener(new PolInterpr(this));
         impartire.addActionListener(new PolInterpr(this));




        mainFrame.setVisible(true);

     }

}
